create table Accounts
(
  ACCOUNT_ID bigint auto_increment
    primary key,
  FIRST_NAME varchar(15) null,
  SECOND_NAME varchar(15) null,
  LAST_NAME varchar(15) null,
  BIRTH_DATE varchar(10) null,
  WORK_PHONE varchar(15) null,
  HOME_ADDRESS varchar(50) null,
  WORK_ADDRESS varchar(50) null,
  EMAIL varchar(20) not null,
  ICQ varchar(10) null,
  SKYPE varchar(15) null,
  INFO tinytext null,
  PASSWORD varchar(60) not null,
  PHOTO longblob null,
  Role varchar(10) not null,
  constraint EMAIL
  unique (EMAIL)
)
;

create table ACCOUNT_FRIENDS
(
  FROM_ID bigint not null,
  TO_ID bigint not null,
  constraint ACCOUNT_FRIENDS_fk0
  foreign key (FROM_ID) references ACCOUNTS (ACCOUNT_ID)
    on delete cascade,
  constraint ACCOUNT_FRIENDS_fk1
  foreign key (TO_ID) references ACCOUNTS (ACCOUNT_ID)
    on delete cascade
)
;

create table ACCOUNT_PHONES
(
  ACCOUNT_ID bigint not null,
  PHONE varchar(20) not null,
  constraint ACCOUNT_PHONES_fk1
  foreign key (ACCOUNT_ID) references ACCOUNTS (ACCOUNT_ID)
    on delete cascade
)
;

create table ACCOUNT_WALL_POSTS
(
  OWNER_ID bigint not null,
  TEXT tinytext null,
  AUTHOR_ID bigint not null,
  TIME timestamp default CURRENT_TIMESTAMP not null,
  POST_ID bigint auto_increment
    primary key
)
;

alter table ACCOUNT_WALL_POSTS
  add constraint ACCOUNT_WALL_POSTS_fk0
foreign key (OWNER_ID) references Accounts (ACCOUNT_ID)
;

alter table ACCOUNT_WALL_POSTS
  add constraint ACCOUNT_WALL_POSTS_fk1
foreign key (AUTHOR_ID) references Accounts (ACCOUNT_ID)
;

create table FRIEND_REQUESTS
(
  from_id bigint null,
  to_id bigint null,
  constraint FRIEND_REQUESTS_fk0
  foreign key (from_id) references Accounts (ACCOUNT_ID)
    on delete cascade,
  constraint FRIEND_REQUESTS_fk1
  foreign key (to_id) references Accounts (ACCOUNT_ID)
    on delete cascade
)
;


create table MESSAGE
(
  MESSAGE_ID bigint auto_increment
    primary key,
  SENDER_ID bigint not null,
  RECIEVER_ID bigint not null,
  TEXT tinytext null,
  TIME timestamp default CURRENT_TIMESTAMP not null,
  constraint MESSAGE_fk0
  foreign key (SENDER_ID) references Accounts (ACCOUNT_ID),
  constraint MESSAGE_fk1
  foreign key (RECIEVER_ID) references Accounts (ACCOUNT_ID)
)
;

create table MESSAGE_CONTACTS
(
  FROM_ID bigint not null,
  TO_ID bigint not null,
  primary key (FROM_ID, TO_ID),
  constraint MESSAGE_CONTACTS_fk0
  foreign key (FROM_ID) references Accounts (ACCOUNT_ID),
  constraint MESSAGE_CONTACTS_fk1
  foreign key (TO_ID) references Accounts (ACCOUNT_ID)
)
;