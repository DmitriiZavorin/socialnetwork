<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="${pageContext.request.contextPath}/webjars/jquery/3.1.0/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/search.js" type="text/javascript"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/search.css"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="navbar" role="navigation">
    <div class="navbar-brand">
        <a href="http://www.getjavajob.com" target="_blank">GetJavaJob</a>
    </div>

    <div class="col-sm-3 col-md-3">
        <div class="ui-widget">
            <form id="custom-search-form" class="navbar-form" role="search" action="/noauth/search" method="post">
                <div class="input-group">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="right">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/noauth/main"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            <c:if test="${account ne null}">
            <li><a href="/auth/edit"><span class="glyphicon glyphicon-edit"></span> Edit profile</a></li>
            <li><form method="POST" action="/logout" hidden id="logoutForm">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                <a href="#" onclick="logout()"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </c:if>
        </ul>
    </div>
</div>