<link rel="stylesheet" href="${pageContext.request.contextPath}/css/wall.css"/>
<script src="${pageContext.request.contextPath}/js/wall.js" type="text/javascript"></script>
<script> var path = '${pageContext.request.contextPath}';</script>
<div class="container" id="wall">
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
    <div class="panel panel-default" id="TypingForm">
        <div class="panel-body">
            <textarea class="form-control counted" id="text" name="message" placeholder="Type in your message" rows="3"
                      style="margin-bottom:10px;"></textarea>
            <h6 class="pull-right" id="counter">320 characters remaining</h6>
            <c:choose>
                <c:when test="${user ne null}">
                    <a type="button" data-ownerId="${user.id}" class="btn btn-primary" id="postAddButton">Post</a>
                </c:when>
                <c:otherwise>
                    <a type="button" data-ownerId="${account.id}" class="btn btn-primary" id="postAddButton">Post</a>
                </c:otherwise>
            </c:choose>
    </div>
    </div>
        <div id="newPost">
        <c:forEach var="post" items="${posts}">
        <div class="panel panel-default" id="${post.id}">
            <div class="panel-heading">
                <div class="block">
                    <img class="media-object photo-profile" src="data:image/jpeg;base64,${post.author.photo}" width="40"
                         height="40">
                </div>
                <div class="block">
                    <a href="/noauth/showUser?userId=${post.author.id}" class="anchor-username inline">
                        <h4 class="media-heading">&nbsp ${post.author.firstName} ${post.author.lastName}</h4>
                    </a>
                </div>
                <c:if test="${post.author.id eq account.id || user eq null}">
                    <div class="block" style="float:right;">
                        <a type="button" class="btn btn-link" data-postId="${post.id}" id="postDeleteButton"><i class="glyphicon glyphicon-remove "></i></a>
                    </div>
                </c:if>
            </div>
            <div class="panel-body">
                <p>${post.text}</p>
            </div>
            <div class="panel-footer">
                <small class="form-text text-muted">${post.time}</small>
            </div>
        </div>
    </c:forEach>
    </div>
    </div>
    </div>
</div>
