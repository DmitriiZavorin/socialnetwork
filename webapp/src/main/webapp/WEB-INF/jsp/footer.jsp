<link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css"/>
<br>
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="col-lg-2 col-xs-12 text-center"></div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="https://www.linkedin.com/in/dmitrii-zavorin-56a5a5150" target="_blank"><i class="fa fa-linkedin fa-2x"></i>Linkedin</a>
            </div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="http://instagram.com/dimazavor" target="_blank"><i class="fa fa-instagram fa-2x"></i>Instagram</a>
            </div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="https://bitbucket.org/DmitriiZavorin/" target="_blank"><i class="fa fa-bitbucket fa-2x"></i>Bitbucket</a>
            </div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="https://t.me/kai42" target="_blank"><i class="fa fa-send fa-2x"></i>Telegram</a>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="text-center">
                <p>Dmitrii Zavorin, GetJavaJob &copy 2017 All rights reserved</p>
            </div>
        </div>
    </div>
</div>