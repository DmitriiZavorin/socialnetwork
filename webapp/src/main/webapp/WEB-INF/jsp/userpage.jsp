<!DOCTYPE html>
<html>
<head>
    <script> var path ='${pageContext.request.contextPath}';</script>
    <title>Login Success Page</title>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <%@ include file="/WEB-INF/jsp/search.jsp"%>
<div class="container">
    <div class="row">
        <div class="col-md-5  toppad  pull-right col-md-offset-3">
            <p class=" text-info">Logged in at ${time}</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">${account.firstName} ${account.secondName} ${account.lastName}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            <c:choose>
                                <c:when test="${not empty photo}">
                                    <img alt="User Pic" src="data:image/jpeg;base64,${photo}" class="img-circle img-responsive"/>
                                </c:when>
                                <c:otherwise>
                                    <img alt="User Pic" src="${pageContext.request.contextPath}/img/defaultUser.jpeg" class="img-circle img-responsive"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>${account.dateOfBirth}</td>
                                </tr>
                                <c:if test="${not empty account.homeAddress}">
                                <tr>
                                    <td>Home Address</td>
                                    <td>${account.homeAddress}</td>
                                </tr>
                                </c:if>
                                <c:if test="${not empty account.workAddress}">
                                <tr>
                                    <td>Work Address</td>
                                    <td>${account.workAddress}</td>
                                </tr>
                                </c:if>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailto:${account.email}">${account.email}</a></td>
                                </tr>
                                <c:if test="${not empty account.workPhone}">
                                <tr>
                                    <td>Work Phone</td>
                                    <td>${account.workPhone}</td>
                                </tr>
                                </c:if>
                                <c:forEach var="phone" items="${account.phones}">
                                    <tr>
                                        <td>Mobile Phone</td>
                                        <td>${phone}</td>
                                    </tr>
                                </c:forEach>
                                <c:if test="${not empty account.skype}">
                                <tr>
                                    <td>Skype</td>
                                    <td>${account.skype}</td>
                                </tr>
                                </c:if>
                                <c:if test="${not empty account.icq}">
                                <tr>
                                    <td>ICQ</td>
                                    <td>${account.icq}</td>
                                </tr>
                                </c:if>
                                <c:if test="${not empty account.additionalInfo}">
                                <tr>
                                    <td>Info</td>
                                    <td>${account.additionalInfo}</td>
                                </tr>
                                </c:if>
                                </tbody>
                            </table>
                            <a href="/noauth/friendsPage?userId=${account.id}" class="btn btn-primary"><i class="fa fa-users" aria-hidden="true"></i> Friends</a>
                            <a href="/auth/showMessagePage" class="btn btn-primary"><i class="glyphicon glyphicon-envelope"></i> Messages</a>
                            <a href="/auth/chatting" class="btn btn-primary"><i class="fa fa-comments" aria-hidden="true"></i> Chat</a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/auth/downloadXML"><i class="glyphicon glyphicon-download"></i> Download XML</a>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<%@ include file="/WEB-INF/jsp/accountWall.jsp"%>
    </div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>
</div>
</body>
</html>