<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/registration.css"/>
    <script> var path = '${pageContext.request.contextPath}';</script>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <%@ include file="/WEB-INF/jsp/search.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-md-10 ">
                    <form class="form-horizontal" action="/noauth/register" method="post" id="submit"
                          enctype="multipart/form-data">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <fieldset>
                            <!-- Form Name  enctype="multipart/form-data" -->
                            <legend>Registration form</legend>

                            <!--First Name-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="First Name">First Name</label>
                                <div class="col-md-4">
                                    <div class="input-group" data-validate="firstName">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input id="First Name" name="firstName" type="text" placeholder="First Name"
                                               class="form-control input-md" required="">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Second Name">Second Name</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input id="Second Name" name="secondName" type="text" placeholder="Second Name"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Last">Last name</label>
                                <div class="col-md-4">
                                    <div class="input-group" data-validate="lastName">
                                        <div class="input-group-addon" style="font-size: 20px;">
                                            <i class="fa fa-male"></i>
                                        </div>
                                        <input id="Last" name="lastName" type="text" placeholder="Last name"
                                               class="form-control input-md" required="">
                                    </div>
                                </div>
                            </div>

                            <!-- File Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Upload photo">Upload photo</label>
                                <div class="col-md-4">
                                    <input id="Upload photo" name="photo" class="input-file" type="file">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Date Of Birth">Date Of Birth</label>
                                <div class="col-md-4">
                                    <div class="input-group" data-validate="date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake" style="font-size: 20px;"></i>
                                        </div>
                                        <input id="Date Of Birth" name="dateOfBirth" type="text"
                                               placeholder="YYYY-MM-DD"
                                               class="form-control input-md" required="">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input Home Address-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Home Address">Home Address</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-home" style="font-size: 20px;"></i>
                                        </div>
                                        <input id="Home Address" name="homeAddress" type="text"
                                               placeholder="Home Address"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>


                            <!-- Text input Work Address-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Work Address">Work Address</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-briefcase" style="font-size: 20px;"></i>
                                        </div>
                                        <input id="Work Address" name="workAddress" type="text"
                                               placeholder="Work Address"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input Work Phone-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Work Phone">Work Phone</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone" style="font-size: 20px;"></i>
                                        </div>
                                        <input id="Work Phone" name="workPhone" type="text" placeholder="Work Phone"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>


                            <!-- Text input Mobile Phone -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Mobile Phone">Mobile Phone</label>
                                <div class="col-md-4" style="padding-left: 30px">
                                    <div class="form-group">

                                        <div class="input-group" data-validate="phone">
                                            <div class="input-group-addon">
                                                <i class="fa fa-mobile-phone" style="font-size: 20px;"></i>
                                            </div>
                                            <input type="text" class="form-control input-md" name="phones"
                                                   id="validate-phone" placeholder="+7##########" style="width: 260px"
                                                   required>
                                            <!--<span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>-->
                                            <span class="input-group-btn danger"><button type="button"
                                                                                         class="btn btn-danger btn-remove"><span
                                                    class="glyphicon glyphicon-minus"></span> Remove</button>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-success btn-sm btn-add btn-add-phone"
                                            style="margin-left: -0.35cm;"><span class="glyphicon glyphicon-plus"></span>
                                        Add
                                    </button>

                                </div>
                            </div>


                            <!-- Text input Email-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Email">Email</label>
                                <div class="col-md-4">
                                    <div class="input-group" data-validate="email">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope-o"></i>

                                        </div>
                                        <input id="Email" name="email" type="text" placeholder="Email"
                                               class="form-control input-md" required="">

                                    </div>

                                </div>
                            </div>

                            <!-- Text input Skype -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Skype">Skype</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-skype"></i>

                                        </div>
                                        <input id="Skype" name="skype" type="text" placeholder="Skype"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input ICQ-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="ICQ">ICQ</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope-o"></i>

                                        </div>
                                        <input id="ICQ" name="icq" type="text" placeholder="ICQ"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input Password-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Password">Password</label>
                                <div class="col-md-4">
                                    <div class="input-group" data-validate="password">
                                        <div class="input-group-addon">
                                            <i class="fa fa-key"></i>
                                        </div>
                                        <input id="Password" name="password" type="text" class="form-control"
                                               placeholder="Password" required="">
                                    </div>
                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Info (max 200 words)">Info (max 200
                                    words)</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" rows="6" id="Info (max 200 words)"
                                              name="additionalInfo"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button class="btn btn-success" type="button" id="myButton" data-toggle="modal"
                                            data-target="#sendMail"><span class="glyphicon glyphicon-thumbs-up"></span>
                                        Register
                                    </button>

                                    <!-- The Modal -->
                                    <div class="modal fade" style="display:none;" id="sendMail">
                                        <div class="modal-dialog" role="dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title"><span
                                                            class="glyphicon glyphicon-user"></span> Confirm
                                                        changes</h4>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" form="submit" class="btn btn-success"
                                                            id="sendData">
                                                        <span class="glyphicon glyphicon-ok"></span> Confirm
                                                    </button>
                                                    <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal"><span
                                                            class="glyphicon glyphicon-remove"></span> Cancel
                                                    </button>
                                                </div>

                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                    <a href="/noauth/register" class="btn btn-danger"><span
                                            class="glyphicon glyphicon-remove-sign"></span> Clear</a>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/footer.jsp" %>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/registration.js" type="text/javascript"></script>
</html>