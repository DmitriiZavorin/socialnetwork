<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/searchResult.css"/>
    <script> var path = '${pageContext.request.contextPath}';</script>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <%@ include file="/WEB-INF/jsp/search.jsp" %>
        <div class="container">
            <div class="row col-md-6 col-md-offset-2 custyle">
                <table class="table table-striped custab" id="results">
                    <c:forEach var="acc" items="${accountsToShow}">
                        <tr>
                            <td><img src="data:image/jpeg;base64,${acc.photo}"
                                     class="media-object photo-profile img-circle" width="60" height="60"/></td>
                            <td><h4><a href="/noauth/showUser?userId=${acc.id}">${acc.firstName} ${acc.lastName}</a></h4></td>
                            <td><a href="mailto:${acc.email}"> ${acc.email}</a></td>
                            <c:if test="${account ne null}">
                                <td class="text-center">
                                    <a class='btn btn-info btn-xs' href="/auth/showMessagePage?userId=${acc.id}">
                                        <span class="glyphicon glyphicon-envelope"></span> message</a>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
        <div class="row btns">
            <ul class="pagination">
                <c:forEach var="index" begin='1' end='${numberOfPages}'>
                    <li class="page"><a type="button" class="btn btn-default" name="${index}">${index}</a></li>
                </c:forEach>
            </ul>
        </div>
        <div id="searchParam" hidden>${search}</div>
        <div id="accountId" hidden>${account.id}</div>
    </div>
    <%@ include file="/WEB-INF/jsp/footer.jsp" %>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/searchResult.js" type="text/javascript"></script>
</html>