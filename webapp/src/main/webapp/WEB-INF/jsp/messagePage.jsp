<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/messagePage.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="wrapper">
    <div class="content">
        <%@ include file="/WEB-INF/jsp/search.jsp" %>
        <div class="container">
            <div class="row">
                <c:choose>
                    <c:when test="${not empty contacts}">
                <div hidden id="accountId">${account.id}</div>
                <div hidden id="userId">${userId}</div>
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading top-bar">
                            <div class="col-md-8 col-xs-8">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-book"></span> Contacts</h3>
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <tbody>
                            <c:forEach var="account" items="${contacts}">
                                <tr id="${account.id}" class="${account.firstName} ${account.lastName}">
                                    <td>
                                        <div class="block">
                                            <img class="media-object photo-profile"
                                                 src="data:image/jpeg;base64,${account.photo}" width="40"
                                                 height="40">
                                        </div>
                                    </td>
                                    <td>${account.firstName} ${account.lastName}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="chatbody">
                        <div class="panel panel-primary">
                            <div class="panel-heading top-bar">
                                <div class="col-md-8 col-xs-8">
                                    <h3 class="panel-title" id="recepientName"> ${userName}</h3>
                                </div>
                            </div>
                            <div class="panel-body msg_container_base">
                                <div id="allMessages">
                                    <c:if test="${messages ne null}">
                                    <c:forEach var="message" items="${messages}">
                                    <c:choose>
                                    <c:when test="${message.senderId eq account.id}">
                                    <div class="row msg_container base_sent" id="singleMessage">
                                        <div class="col-md-10 col-xs-10">
                                            <div class="messages msg_sent">
                                                </c:when>
                                                <c:otherwise>
                                                <div class="row msg_container base_receive" id="singleMessage">
                                                    <div class="col-md-10 col-xs-10">
                                                        <div class="messages msg_receive">
                                                            </c:otherwise>
                                                            </c:choose>
                                                            <p>${message.text}</p>
                                                            <time>${message.time}</time>
                                                        </div>
                                                    </div>
                                                </div>
                                                </c:forEach>
                                                </c:if>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="input-group">
                                                <input id="textMessage" type="text"
                                                       class="form-control input-sm chat_input"
                                                       placeholder="Write your message here..."/>
                                                <span class="input-group-btn">
                                        <button class="btn btn-primary btn" id="sendMsg"><i class="fa fa-send fa-1x"
                                                                                            aria-hidden="true"></i></button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-info">
                        You have no contacts yet, add some via search.
                    </div>
                </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/footer.jsp" %>
</div>
</body>
<script src="${pageContext.request.contextPath}/webjars/sockjs-client/sockjs.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/stomp-websocket/stomp.min.js"></script>
<script src="${pageContext.request.contextPath}/js/messagePage.js" type="text/javascript" charset="UTF-8"></script>
<script> var path = '${pageContext.request.contextPath}';</script>
</html>