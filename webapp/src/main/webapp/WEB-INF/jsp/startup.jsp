<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SocialNetwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../css/startup.css">
    <script> var path ='${pageContext.request.contextPath}';</script>
</head>
<body>
<div class="wrapper">
    <div class="content">
<%@ include file="/WEB-INF/jsp/search.jsp"%>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Welcome! Sign in to continue</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
                <form class="form-signin" action="/login" method="post">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required autofocus>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <label class="checkbox pull-left"><input type="checkbox" name="remember-me" id="remember-me" value="true">Remember me</label>
                    <a href="https://stackoverflow.com" target="_blank" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
            <a href="/noauth/register" class="text-center new-account">Create account </a>
        </div>
    </div>
</div>
    </div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>
</div>
</body>
</html>