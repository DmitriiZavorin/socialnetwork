<!DOCTYPE html>
<html>
<head>
    <title>Login Success Page</title>
    <script> var path = '${pageContext.request.contextPath}';</script>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <%@ include file="/WEB-INF/jsp/search.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">${user.firstName} ${user.secondName} ${user.lastName}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 " align="center">
                                    <c:choose>
                                        <c:when test="${not empty userPhoto}">
                                        <img alt="User Pic" src="data:image/jpeg;base64,${userPhoto}" class="img-circle img-responsive"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img alt="User Pic" src="${pageContext.request.contextPath}/img/defaultUser.jpeg" class="img-circle img-responsive"/>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class=" col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td>${user.dateOfBirth}</td>
                                        </tr>
                                        <c:if test="${not empty user.homeAddress}">
                                            <tr>
                                                <td>Home Address</td>
                                                <td>${user.homeAddress}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${not empty user.workAddress}">
                                            <tr>
                                                <td>Work Address</td>
                                                <td>${user.workAddress}</td>
                                            </tr>
                                        </c:if>
                                        <tr>
                                            <td>Email</td>
                                            <td><a href="mailto:${user.email}">${user.email}</a></td>
                                        </tr>
                                        <c:if test="${not empty user.workPhone}">
                                            <tr>
                                                <td>Work Phone</td>
                                                <td>${user.workPhone}</td>
                                            </tr>
                                        </c:if>
                                        <c:forEach var="phone" items="${user.phones}">
                                            </tr>
                                            <td>Mobile Phone</td>
                                            <td>${phone}</td>
                                            </tr>
                                        </c:forEach>
                                        <c:if test="${not empty user.skype}">
                                            <tr>
                                                <td>Skype</td>
                                                <td>${user.skype}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${not empty user.icq}">
                                            <tr>
                                                <td>ICQ</td>
                                                <td>${user.icq}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${not empty user.additionalInfo}">
                                            <tr>
                                                <td>Info</td>
                                                <td>${user.additionalInfo}</td>
                                            </tr>
                                        </c:if>
                                        </tbody>
                                    </table>
                                    <a href="/noauth/friendsPage?userId=${user.id}" class="btn btn-primary"><i
                                            class="fa fa-users" aria-hidden="true"></i> Friends List</a>
                                    <c:if test="${account ne null}">
                                        <a href="/auth/showMessagePage?userId=${user.id}" class="btn btn-primary"><i
                                                class="glyphicon glyphicon-envelope"></i> Write Message</a>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <c:if test="${account ne null && account.id ne user.id}">
                            <div class="panel-footer">
                                <c:set var="status" value="addRequest"/>
                                <c:set var="text" value="add friend"/>
                                <c:forEach var="item" items="${ownRequests}">
                                    <c:if test="${item.id eq user.id}">
                                        <c:set var="status" value="deleteRequest"/>
                                        <c:set var="text" value="cancel friendship request"/>
                                    </c:if>
                                </c:forEach>
                                <c:forEach var="item" items="${friends}">
                                    <c:if test="${item.id eq user.id}">
                                        <c:set var="status" value="deleteFriend"/>
                                        <c:set var="text" value="remove friend"/>
                                    </c:if>
                                </c:forEach>
                                <c:forEach var="item" items="${friendsRequests}">
                                    <c:if test="${item.id eq user.id}">
                                        <c:set var="status" value="addFriend"/>
                                        <c:set var="text" value="confirm request"/>
                                    </c:if>
                                </c:forEach>
                                <a onclick="submitForm()" href="/auth/${status}?userId=${user.id}" name="${user.id}"
                                   id="friendButton" class="btn btn-primary"><span
                                        class="glyphicon glyphicon-user"></span> ${text}</a>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <c:if test="${account ne null}">
            <%@ include file="/WEB-INF/jsp/accountWall.jsp" %>
        </c:if>
    </div>
    <%@ include file="/WEB-INF/jsp/footer.jsp" %>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/user.js" type="text/javascript"></script>
</html>