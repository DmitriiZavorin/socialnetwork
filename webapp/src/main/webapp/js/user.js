function submitForm() {
        event.preventDefault();
        var c = $('#friendButton');
        var id = c.attr("name");
        var href = c.attr("href");
        var http = new XMLHttpRequest();
        http.open("GET", href, true);
        http.send();
        if (href.startsWith("/auth/addRequest")) {
            c.attr("href", "/auth/deleteRequest" + "?userId=" + id);
            c.text("cancel friendship request");
        } else if (href.startsWith("/auth/deleteRequest")) {
            c.attr("href", "/auth/addRequest" + "?userId=" + id);
            c.text("add friend");
        } else if (href.startsWith("/auth/deleteFriend")) {
            c.attr("href", "/auth/addRequest" + "?userId=" + id);
            c.text("add friend");
        } else if (href.startsWith("/auth/addFriend")) {
            c.attr("href", "/auth/deleteRequest" + "?userId=" + id);
            c.text("delete friend");
        }
};