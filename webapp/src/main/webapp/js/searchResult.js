$(document).ready(
    $(document).on('click', '.page', function () {
        var isAuthenticated = $("#accountId").text();
        var pageNum = $(this).find('a').attr('name');
        var search = document.getElementById("searchParam").innerHTML;
        $.ajax({
            url: path + '/noauth/page',
            dataType: 'json',
            data: "page=" + pageNum + "&search=" + search,
            success: function (data) {
                $('#results').empty();
                $.each(data, function (i, acc) {
                    if (isAuthenticated != 0) {
                        $('#results').append(
                            "<tr>" +
                            "<td><img src=\"data:image/jpeg;base64," + acc.photo + "\" class=\"media-object photo-profile img-circle\" width=\"60\" height=\"60\"/></td>" +
                            "    <td><h4><a href=\"showUser?userId=" + acc.id + "\">" + acc.firstName + " " + acc.lastName + "</a></h4></td>" +
                            "    <td><a href=\"mailto:" + acc.email + "\"> " + acc.email + "</a></td>" +
                            "    <td class=\"text-center\">" +
                            "    <a class='btn btn-info btn-xs' href=\"/auth/showMessagePage?userId=" + acc.id + "\">" +
                            "    <span class=\"glyphicon glyphicon-envelope\"></span> message</a>" +
                            "    </td></tr>"
                        );
                    } else {
                        $('#results').append(
                        "<tr>" +
                        "<td><img src=\"data:image/jpeg;base64," + acc.photo + "\" class=\"media-object photo-profile img-circle\" width=\"60\" height=\"60\"/></td>" +
                        "    <td><h4><a href=\"showUser?userId=" + acc.id + "\">" + acc.firstName + " " + acc.lastName + "</a></h4></td>" +
                        "    <td><a href=\"mailto:" + acc.email + "\"> " + acc.email + "</a></td></tr>"
                        );
                    }
                });
            }
        });
    })
);