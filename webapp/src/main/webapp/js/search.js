$(document).ready(function() {
    $("#search").autocomplete({
        source: function (request, response) {
            $.get(path + "/noauth//search?search=" + request.term, function(data) {
                    response($.map(data, function (account, i) {
                        return {value: request.term, id: account.id, label: account.firstName + " " + account.lastName}
                    }));
            });
        },
        autoFocus: true,
        select: function(event, ui) {
        window.location = path + '/noauth/showUser?userId=' + ui.item.id;
        },
        minLength: 2
    });
});

function logout() {
    event.preventDefault();
    $("#logoutForm").submit();
}