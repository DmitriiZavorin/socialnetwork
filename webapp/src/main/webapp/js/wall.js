$(document).ready(
    $(document).on('click', 'a', function () {
        if (this.id == 'postDeleteButton') {
            var id = $(this).attr('data-postId');
            $.ajax({
                url: path + "/auth/deleteAccountWallPost",
                dataType: 'json',
                data: {postId: id}
            });
            $("#" + id).remove();
        } else if (this.id == 'postAddButton') {
            var ownerId = $(this).attr('data-ownerId');
            var text = $('#text').val();
            $.ajax({
                url: path + "/auth/addPostToAccountWall",
                dataType: 'json',
                data: {ownerId: ownerId, text: text},
                success: function (data) {
                    $('#text').val("");
                    $('#newPost').prepend(
                        "<div class=\"panel panel-default\" id=\"" + data.id + "\"><div class=\"panel-heading\">" +
                        "                <div class=\"block\">" +
                        "                    <img class=\"media-object photo-profile\" src=\"data:image/jpeg;base64," + data.author.photo + "\" width=\"40" +
                        "                         height=\"40\"></div>" +
                        "                <div class=\"block\">" +
                        "                    <a href=\"showUser?userId=${post.author.id}\" class=\"anchor-username inline\">" +
                        "                        <h4 class=\"media-heading\">&nbsp" + data.author.firstName + " " + data.author.lastName + "</h4>" +
                        "                    </a>" +
                        "                </div>" +
                        "                    <div class=\"block\" style=\"float:right;\">" +
                        "                        <a type=\"button\" class=\"btn btn-link\" data-postId=\"" + data.id + "\" id=\"postDeleteButton\"><i class=\"glyphicon glyphicon-remove \"></i></a>" +
                        "                    </div>" +
                        "            </div>" +
                        "            <div class=\"panel-body\">" +
                        "                <p>" + data.text + "</p>" +
                        "            </div><div class=\"panel-footer\">" +
                        "                <small class=\"form-text text-muted\">" + data.time + "</small></div></div>"
                    );
                }
            });
        }
    }),


    /**
     *
     * jquery.charcounter.js version 1.2
     * requires jQuery version 1.2 or higher
     * Copyright (c) 2007 Tom Deater (http://www.tomdeater.com)
     * Licensed under the MIT License:
     * http://www.opensource.org/licenses/mit-license.php
     *
     */
    (function ($) {
        $.fn.charCounter = function (max, settings) {
            max = max || 100;
            settings = $.extend({
                container: "<span></span>",
                classname: "charcounter",
                format: "(%1 characters remaining)",
                pulse: true,
                delay: 0
            }, settings);
            var p, timeout;

            function count(el, container) {
                el = $(el);
                if (el.val().length > max) {
                    el.val(el.val().substring(0, max));
                    if (settings.pulse && !p) {
                        pulse(container, true);
                    }

                }

                if (settings.delay > 0) {
                    if (timeout) {
                        window.clearTimeout(timeout);
                    }
                    timeout = window.setTimeout(function () {
                        container.html(settings.format.replace(/%1/, (max - el.val().length)));
                    }, settings.delay);
                } else {
                    container.html(settings.format.replace(/%1/, (max - el.val().length)));
                }
            }

            function pulse(el, again) {
                if (p) {
                    window.clearTimeout(p);
                    p = null;
                }

                el.animate({opacity: 0.1}, 100, function () {
                    $(this).animate({opacity: 1.0}, 100);
                });
                if (again) {
                    p = window.setTimeout(function () {
                        pulse(el)
                    }, 200);
                }
            }

            return this.each(function () {
                var container;
                if (!settings.container.match(/^<.+>$/)) {
                    // use existing element to hold counter message
                    container = $(settings.container);
                } else {
                    // append element to hold counter message (clean up old element first)
                    $(this).next("." + settings.classname).remove();
                    container = $(settings.container)
                        .insertAfter(this)
                        .addClass(settings.classname);
                }
                $(this)
                    .unbind(".charCounter")
                    .bind("keydown.charCounter", function () {
                        count(this, container);
                    })
                    .bind("keypress.charCounter", function () {
                        count(this, container);
                    })
                    .bind("keyup.charCounter", function () {
                        count(this, container);
                    })
                    .bind("focus.charCounter", function () {
                        count(this, container);
                    })
                    .bind("mouseover.charCounter", function () {
                        count(this, container);
                    })
                    .bind("mouseout.charCounter", function () {
                        count(this, container);
                    })
                    .bind("paste.charCounter", function () {
                        var me = this;
                        setTimeout(function () {
                            count(me, container);
                        }, 10);
                    });
                if (this.addEventListener) {
                    this.addEventListener('input', function () {
                        count(this, container);
                    }, false);
                }

                count(this, container);
            });
        };

    })(jQuery),


    $(function () {
        $(".counted").charCounter(320, {container: "#counter"});
    })
);