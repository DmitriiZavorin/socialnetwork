$(document).ready(function() {

    $('.input-group input[required]').on('keyup change', function() {
        var $form = $(this).closest('form'),
            $group = $(this).closest('.input-group'),
            $addon = $group.find('.input-group-addon'),
            //$icon = $addon.find('span'),
            state = false;

        if ($group.data('validate') == 'phone') {
            state = /^\+7[0-9]{3}-?[0-9]{6,12}$/.test($(this).val())
        } else if ($group.data('validate') == 'email') {
            state = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($(this).val())
        } else if ($group.data('validate') == 'password') {
            state = $(this).val() ? true : false;
        } else if ($group.data('validate') == 'date') {
            state = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test($(this).val());
        } else if ($group.data('validate') == 'firstName') {
            state = $(this).val().length >= 3;
        } else if ($group.data('validate') == 'lastName') {
            state = $(this).val().length >= 3;
        }

        if (state) {
            $addon.removeClass('danger');
            $addon.addClass('success');
            //$icon.attr('class', 'glyphicon glyphicon-ok');
        } else {
            $addon.removeClass('success');
            $addon.addClass('danger');
            //$icon.attr('class', 'glyphicon glyphicon-remove');
        }

        if ($form.find('.input-group-addon.danger').length == 0) {
            $("#myButton").prop('disabled', false);
        } else {
            $("#myButton").prop('disabled', true);
        }
    });

    var addFormGroup = function (event) {
        event.preventDefault();
        var $formGroup = $(this).prev('.form-group');
        var $formGroupClone = $formGroup.clone(true);
        var $group = $formGroupClone.find('.input-group');
        var $addon = $group.find('.input-group-addon');
        var $form = $(this).closest('form');

        $formGroupClone.find(".form-control").val("");
        //$addon.removeClass('danger');
        $addon.removeClass('success');
        $addon.addClass('danger');
        $formGroupClone.insertAfter($formGroup);

        if ($form.find('.input-group-addon.danger').length == 0) {
            $("#myButton").prop('disabled', false);
        } else {
            $("#myButton").prop('disabled', true);
        }

    };

    var removeFormGroup = function (event) {
        event.preventDefault();
        var $formGroup = $(this).closest('.form-group');
        var $form = $(this).closest('form');

        if ($("input[id='validate-phone']").length > 1) {
            $formGroup.remove()
        }

        if ($form.find('.input-group-addon.danger').length == 0) {
            $("#myButton").prop('disabled', false);
        } else {
            $("#myButton").prop('disabled', true);
        }

    };

    $('.input-group input[required]').trigger('change');
    $(document).on('click', '.btn-add', addFormGroup);
    $(document).on('click', '.btn-remove', removeFormGroup);

});