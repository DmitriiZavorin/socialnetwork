$(document).ready(
    $(document).on('click', '#friends_list, #requests_list, #own_requests_list', function () {
        var c = $(this).attr('name');
        var id = $('#id').text();
        $.ajax({
            url: path + "/auth/" + c,
            dataType: 'json',
            data: "userId=" + id,
            success: function (data) {
                $('#results').empty();
                $.each(data, function (i, acc) {
                    $('#results').append(
                        "<tr>" +
                        "<td><img src=\"data:image/jpeg;base64," + acc.photo + "\" class=\"media-object photo-profile img-circle\" width=\"60\" height=\"60\"/></td>" +
                        "    <td><h4><a href=\"showUser?userId=" + acc.id + "\">" + acc.firstName + " " + acc.lastName + "</a></h4></td>" +
                        "    <td><a href=\"mailto:" + acc.email + "\"> " + acc.email + "</a></td>" +
                        "    <td class=\"text-center\">" +
                        "    <a class='btn btn-info btn-xs' href=\"/auth/showMessagePage?userId=" + acc.id + "\">" +
                        "    <span class=\"glyphicon glyphicon-envelope\"></span> message</a>" +
                        "    </td></tr>"
                    );
                });
            }
        });
    })
);