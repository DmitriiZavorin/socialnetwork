$(document).ready(function () {
    $(document).on('click', 'tr', function () {
        userId = $(this).attr('id');
        $("#userId").text(userId);
        recieverId = userId;
        $('#recepientName').text($(this).attr('class'));
        $.ajax({
            url: path + "/auth/showMessages",
            dataType: 'json',
            data: {userId: userId},
            success: function (data) {
                var all = $('#allMessages');
                all.empty();
                $.each(data, function (i, message) {
                    if (message.senderId == userId) {
                        all.append(leftMessage() + messagebody(message.text, message.time));
                    } else {
                        all.append(rightMessage() + messagebody(message.text, message.time));
                    }
                })
            }
        });
        connect();
    });


    $("#sendMsg").click(function () {
        function sendMessage() {
            var text = $("#textMessage");
                stompClient.send("/app/" + getPrivateChat(senderId, recieverId), {},
                    JSON.stringify({'senderId': senderId, 'recieverId': recieverId, 'text': text.val()}));
            text.val("")
        }
        //alert(getPrivateChat(senderId, recieverId));
        sendMessage();
    });

    $("#textMessage").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#sendMsg").click();
        }
    });

    if (recieverId != null) {
        connect();
    }

});

var userId;
var recieverId = $("#userId").text();
var stompClient = null;
var accountId, senderId = $('#accountId').text();

function leftMessage() {
    return "<div class=\"row msg_container base_receive\" id=\"singleMessage\">"
        + "<div class=\"col-md-10 col-xs-10\">"
        + "<div class=\"messages msg_receive\">";
}

function rightMessage() {
    return "<div class=\"row msg_container base_sent\" id=\"singleMessage\">"
        + "<div class=\"col-md-10 col-xs-10\">"
        + "<div class=\"messages msg_sent\">";
}

function messagebody(text, date) {
    return "<p>" + text + "</p><time>" + date + "</time></div></div></div>";
}

function getPrivateChat(senderId, recieverId) {
    return senderId < recieverId ? senderId + "/" + recieverId : recieverId + "/" + senderId;
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function connect() {
    disconnect();
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);
    event.preventDefault();
}

function onConnected() {
    stompClient.subscribe('/channel/private/' + getPrivateChat(senderId, recieverId), function (message) {
        showMessage(JSON.parse(message.body));
    })
}

function showMessage(message) {
    //console.log(message.recieverId + "/" + $('#accountId').text());
    if (message.recieverId != $('#accountId').text()) {
        $('#allMessages').append(rightMessage() + messagebody(message.text, message.time));
    } else {
        $('#allMessages').append(leftMessage() + messagebody(message.text, message.time));
    }
}

function onError(error) {
    $('#allMessages').append(
        '<h3 style="color:red">Could not connect to WebSocket server. Please refresh this page to try again!</h3>');
}