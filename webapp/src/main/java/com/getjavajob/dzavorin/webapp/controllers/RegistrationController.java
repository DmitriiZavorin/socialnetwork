package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.CurrentAccount;
import com.getjavajob.dzavorin.common.model.Role;
import com.getjavajob.dzavorin.service.impl.AccountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
public class RegistrationController implements Creator {

    private static final Logger logger = LogManager.getLogger(RegistrationController.class);

    private AccountServiceImpl accountServiceImpl;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(AccountServiceImpl accountServiceImpl, PasswordEncoder passwordEncoder) {
        this.accountServiceImpl = accountServiceImpl;
        this.passwordEncoder = passwordEncoder;

    }

    @PostMapping("/noauth/register")
    public String doRegister(@ModelAttribute Account account,
                             BindingResult result,
                             Model model) {
        if (accountServiceImpl.getUserByEmail(account.getEmail()) != null) {
            logger.debug("Account with specified username {} already exists", account.getEmail());
            return "registration";
        } else {
            account.setRole(Role.USER);
            CurrentAccount currentAccount = new CurrentAccount(account);
            String password = account.getPassword();
            account.setPassword(passwordEncoder.encode(account.getPassword()));
            account = accountServiceImpl.create(account);
            logger.debug("successfully created new account");
            return "forward:/login?email=" + account.getEmail()
                    + "&password=" + password;
//            Authentication authentication = new PreAuthenticatedAuthenticationToken(currentAccount, currentAccount.getPassword());
//            SecurityContextHolder.getContext().setAuthentication(authentication);
//            mav.setViewName("userpage");
//            mav.addObject("account", account);
//            mav.addObject("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
//            mav.addObject("time", LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME));
        }
    }

    @GetMapping("/noauth/register")
    public String redirectRegistrarion() {
        logger.debug("/register GET request");
        return "registration";
    }
}