package com.getjavajob.dzavorin.webapp.controllers;

import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.util.ArrayList;

public interface Creator {

    @InitBinder("account")
    default void customModel(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, "dateOfBirth", new PropertyEditorSupport() {
            @Override
            public String getAsText () {
                LocalDate ld = (LocalDate) getValue () ;
                return ld.toString () ;
            }
            @Override
            public void setAsText(String text) throws IllegalArgumentException{
                setValue(LocalDate.parse(text));
            }
        });
        binder.registerCustomEditor(ArrayList.class, "phones", new CustomCollectionEditor(ArrayList.class));
        binder.registerCustomEditor(byte[].class, "photo", new ByteArrayMultipartFileEditor());
    }
}