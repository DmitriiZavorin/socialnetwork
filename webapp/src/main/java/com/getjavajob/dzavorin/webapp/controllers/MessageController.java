package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.dto.AccountDto;
import com.getjavajob.dzavorin.common.dto.MessageDto;
import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.Message;
import com.getjavajob.dzavorin.service.impl.AccountServiceImpl;
import com.getjavajob.dzavorin.service.impl.AccountMessageServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Set;

@Controller
public class MessageController {

    private static final Logger logger = LogManager.getLogger(MessageController.class);

    private AccountServiceImpl accountServiceImpl;
    private AccountMessageServiceImpl accountMessageServiceImpl;

    @Autowired
    public MessageController(AccountServiceImpl accountServiceImpl, AccountMessageServiceImpl accountMessageServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
        this.accountMessageServiceImpl = accountMessageServiceImpl;
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/channel/public")
    public Message sendMessage(@Payload Message chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/channel/public")
    public Message addUser(@Payload Message chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSenderName());
        return chatMessage;
    }

    @MessageMapping("/{senderId}/{recieverId}")
    @SendTo("/channel/private/{senderId}/{recieverId}")
    public MessageDto send(@Payload Message message) throws Exception {
        logger.warn(message.toString());
        return accountMessageServiceImpl.saveMessage(message);
    }

    @GetMapping("/auth/showMessages")
    @ResponseBody
    public List<MessageDto> showMessages(@SessionAttribute("account") Account account,
                                        @RequestParam("userId") Long userId) {
        logger.warn("contact to messages userId:" + userId);
        return accountMessageServiceImpl.getMessages(accountServiceImpl.getById(userId), account);

    }

    @GetMapping("/auth/showMessagePage")
    public ModelAndView showMessagePage(@SessionAttribute("account") Account account,
                                        @RequestParam(name = "userId", required = false) Long userId) {
        ModelAndView mav = new ModelAndView("messagePage");
        Set<AccountDto> contacts = accountServiceImpl.getContacts(accountServiceImpl.getById(account.getId()));
        if (userId != null && userId != account.getId()) {
            if (contacts.stream().noneMatch(c -> c.getId() == userId)) {
                contacts.add(accountServiceImpl.addContact(userId, account));
            }
            Account user = accountServiceImpl.getById(userId);
            mav.addObject("userName", user.getFirstName() + " " + user.getLastName());
            mav.addObject("userId", userId);
            mav.addObject("messages", accountMessageServiceImpl.getMessages(user, account));
        }
        mav.addObject("contacts", contacts);
        return mav;
    }
}