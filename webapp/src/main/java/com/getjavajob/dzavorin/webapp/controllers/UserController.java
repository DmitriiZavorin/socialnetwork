package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.service.impl.AccountServiceImpl;
import com.getjavajob.dzavorin.service.impl.AccountWallPostServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Base64;

@Controller
@SessionAttributes({"account", "photo", "time"})
public class UserController implements Creator {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    private AccountServiceImpl accountServiceImpl;
    private AccountWallPostServiceImpl accountWallPostServiceImpl;

    @Autowired
    public UserController(AccountServiceImpl accountServiceImpl,
                          AccountWallPostServiceImpl accountWallPostServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
        this.accountWallPostServiceImpl = accountWallPostServiceImpl;
    }

    @GetMapping("/noauth/showUser")
    public ModelAndView showUser(@SessionAttribute(value = "account", required = false) Account account,
                                 @RequestParam("userId") long id) {
        ModelAndView mav = new ModelAndView();
        if (account != null && account.getId() == id) {
            mav.addObject("posts", accountWallPostServiceImpl.getPosts(account));
            logger.debug("/userPage GET request");
            mav.setViewName("userpage");
            return mav;
        }
        Account user = accountServiceImpl.getById(id);
        mav.setViewName("user");
        mav.addObject("user", user);
        mav.addObject("userPhoto", Base64.getEncoder().encodeToString(user.getPhoto()));
        logger.debug("show userPage" + user.getId());
        if (account != null) {
            Account acc = accountServiceImpl.getById(account.getId());
            mav.addObject("posts", accountWallPostServiceImpl.getPosts(user));
            mav.addObject("ownRequests", accountServiceImpl.getOwnRequests(acc));
            mav.addObject("friendsRequests", accountServiceImpl.getFriendsRequests(acc));
            mav.addObject("friends", accountServiceImpl.getFriends(acc));
        }
        return mav;
    }

    @PostMapping("/auth/edit")
    public ModelAndView doEdit(@SessionAttribute("account") Account oldAccount,
                               @ModelAttribute("account") Account newAccount,
                               BindingResult bindingResult, Model model) {
        oldAccount = accountServiceImpl.getById(oldAccount.getId());
        oldAccount = accountServiceImpl.update(oldAccount, newAccount);
        logger.debug(oldAccount.getId() + " successfully updated");
        ModelAndView mav = new ModelAndView("userpage");
        mav.addObject("account", oldAccount);
        mav.addObject("posts", accountWallPostServiceImpl.getPosts(oldAccount));
        mav.addObject("photo", Base64.getEncoder().encodeToString(oldAccount.getPhoto()));
        return mav;
    }

    @GetMapping("/noauth/main")
    public ModelAndView redirectUserPage(@SessionAttribute(value = "account", required = false) Account account,
                                         @SessionAttribute(value = "time", required = false) String time) {
        ModelAndView mav = new ModelAndView();
        if (account != null) {
            mav.addObject("posts", accountWallPostServiceImpl.getPosts(account));
            logger.debug("/userPage GET request");
            mav.setViewName("userpage");
        } else {
            mav.setViewName("startup");
        }
        return mav;
    }

    @GetMapping("/auth/edit")
    public String redirectEdit() {
        logger.debug("/edit GET request");
        return "editUser";
    }

    @GetMapping("/auth/chatting")
    public String redirectChat() {
        logger.debug("/chatting GET request");
        return "chat";
    }
}