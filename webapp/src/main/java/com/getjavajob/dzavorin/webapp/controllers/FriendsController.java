package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.dto.AccountDto;
import com.getjavajob.dzavorin.service.impl.AccountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
public class FriendsController {

    private static final Logger logger = LogManager.getLogger(FriendsController.class);

    private AccountServiceImpl accountServiceImpl;

    @Autowired
    public FriendsController(AccountServiceImpl accountServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
    }

    @GetMapping("/noauth/friendsPage")
    public ModelAndView friendsPage(@SessionAttribute(value = "account", required = false) Account account,
                                    @RequestParam("userId") Long id) {
        ModelAndView mav = new ModelAndView("showFriends");
        if (account != null) {
            mav.addObject("account", account);
        }
        mav.addObject("accountsToShow", accountServiceImpl.getFriends(accountServiceImpl.getById(id)));
        mav.addObject("id", id);
        return mav;
    }

    @GetMapping("/auth/deleteFriend")
    @ResponseBody
    public void deleteFriend(@SessionAttribute("account") Account account,
                             @RequestParam("userId") Long id,
                             Model model) {
        model.addAttribute("account",
                accountServiceImpl.deleteFriend(accountServiceImpl.getById(account.getId()), accountServiceImpl.getById(id)));
    }

    @GetMapping("/auth/deleteRequest")
    @ResponseBody
    public void deleteRequest(@SessionAttribute("account") Account account,
                              @RequestParam("userId") Long id,
                              Model model) {
        model.addAttribute("account",
                accountServiceImpl.deleteRequest(accountServiceImpl.getById(account.getId()), accountServiceImpl.getById(id)));
    }

    @GetMapping("/auth/addRequest")
    @ResponseBody
    public void addRequest(@SessionAttribute("account") Account account,
                           @RequestParam("userId") Long id,
                           Model model) {
        model.addAttribute("account",
                accountServiceImpl.addRequest(accountServiceImpl.getById(account.getId()), accountServiceImpl.getById(id)));
    }

    @GetMapping("/auth/addFriend")
    @ResponseBody
    public void addFriend(@SessionAttribute("account") Account account,
                          @RequestParam("userId") Long id,
                          Model model) {
        model.addAttribute("account",
                accountServiceImpl.addFriend(accountServiceImpl.getById(account.getId()), accountServiceImpl.getById(id)));
    }

    @GetMapping("/auth/friends")
    @ResponseBody
    public Set<AccountDto> showFriends(@RequestParam("userId") Long id) {
        Set<AccountDto> accountsToShow = accountServiceImpl.getFriends(accountServiceImpl.getById(id));
        logger.debug("friends is empty:" + accountsToShow.isEmpty());
        return accountsToShow;
    }

    @GetMapping("/auth/friendsRequests")
    @ResponseBody
    public Set<AccountDto> showFriendsRequests(@RequestParam("userId") Long id) {
        Set<AccountDto> accountsToShow = accountServiceImpl.getFriendsRequests(accountServiceImpl.getById(id));
        logger.debug("requests is empty" + accountsToShow.isEmpty());
        return accountsToShow;
    }

    @GetMapping("/auth/ownRequests")
    @ResponseBody
    public Set<AccountDto> showOwnRequests(@RequestParam("userId")  Long id) {
        Set<AccountDto> accountsToShow = accountServiceImpl.getOwnRequests(accountServiceImpl.getById(id));
        logger.debug("Own requests is empty:" + accountsToShow.isEmpty());
        return accountsToShow;
    }
}