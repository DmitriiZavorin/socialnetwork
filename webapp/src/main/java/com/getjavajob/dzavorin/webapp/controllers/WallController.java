package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.dto.WallPostDto;
import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.service.impl.AccountServiceImpl;
import com.getjavajob.dzavorin.service.impl.AccountWallPostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

@RestController
public class WallController {

    private AccountWallPostServiceImpl wps;
    private AccountServiceImpl as;

    @Autowired
    public WallController(AccountWallPostServiceImpl wps, AccountServiceImpl as) {
        this.as = as;
        this.wps = wps;
    }

    @GetMapping("/auth/addPostToAccountWall")
    public WallPostDto addPostToAccountWall(@SessionAttribute("account") Account account,
                                            @RequestParam("text") String text,
                                            @RequestParam("ownerId") Long ownerId) {
        return wps.addPost(account, ownerId, text);
    }

    @GetMapping("/auth/deleteAccountWallPost")
    public void deletePostFromAccountWall(@RequestParam("postId") Long postId) {
        wps.deletePost(postId);
    }
}