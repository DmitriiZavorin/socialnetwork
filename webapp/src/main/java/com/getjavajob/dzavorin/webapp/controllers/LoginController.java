package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.CurrentAccount;
import com.getjavajob.dzavorin.service.impl.AccountWallPostServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

@Controller
@SessionAttributes({"account", "photo", "time"})
public class LoginController {

    private static final Logger logger = LogManager.getLogger(LoginController.class);

    private AccountWallPostServiceImpl accountWallPostServiceImpl;

    public LoginController(AccountWallPostServiceImpl accountWallPostServiceImpl) {
        this.accountWallPostServiceImpl = accountWallPostServiceImpl;
    }

    @GetMapping("/")
    public ModelAndView startup(@AuthenticationPrincipal CurrentAccount currentAccount) {
        ModelAndView mav = new ModelAndView();
        if (currentAccount != null) {
            mav.setViewName("userpage");
            mav.addObject("posts", accountWallPostServiceImpl.getPosts(currentAccount.getAccount()));
            return mav;
        }
        mav.setViewName("startup");
        return mav;
    }

    @GetMapping("/auth")
    public ModelAndView showMainUser(@AuthenticationPrincipal CurrentAccount currentAccount) {
        if (currentAccount != null) {
            Account account = currentAccount.getAccount();
            logger.debug("loged in");
            ModelAndView mav = new ModelAndView("userpage");
            mav.addObject("account", account);
            mav.addObject("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
            mav.addObject("posts", accountWallPostServiceImpl.getPosts(account));
            mav.addObject("time", LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME));
            return mav;
        }
        logger.debug("unregistered login attemp:");
        return new ModelAndView("startup");
    }
}