package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.dto.AccountDto;
import com.getjavajob.dzavorin.service.impl.AccountSearchServiceImpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

import static com.getjavajob.dzavorin.service.utils.AccountDtoMapper.mapToList;

@Controller
public class SearchController {

    private static final Logger logger = LogManager.getLogger(SearchController.class);

    private AccountSearchServiceImpl accountSearchServiceImpl;

    @Autowired
    public SearchController(AccountSearchServiceImpl accountSearchServiceImpl) {
        this.accountSearchServiceImpl = accountSearchServiceImpl;
    }

    @PostMapping("/noauth/search")
    public ModelAndView searchAccounts(@RequestParam("search")  String search) {
        ModelAndView mav = new ModelAndView("searchResult");
        Page<Account> accounts = accountSearchServiceImpl.getBySearchReq(search, 0);
        List<AccountDto> accountsToShow = mapToList(accounts.getContent());
        logger.debug("Search result:" + accountsToShow.isEmpty());
        mav.addObject("numberOfPages", accounts.getTotalPages());
        mav.addObject("accountsToShow", accountsToShow);
        mav.addObject("search", search);
        return mav;
    }

    @GetMapping("/noauth/search")
    @ResponseBody
    public List<AccountDto> searchAutocomplete(@RequestParam("search") String search) {
        logger.debug("Search request: " + search);
        return accountSearchServiceImpl.getNamesBySearchReq(search);
    }

    @GetMapping("/noauth/page")
    @ResponseBody
    public List<AccountDto> nextPage(@RequestParam String search, @RequestParam int page) {
        logger.debug("Page: " + page + ", " + "search: " + search );
        return mapToList(accountSearchServiceImpl.getBySearchReq(search, page - 1).getContent());
    }

}