package com.getjavajob.dzavorin.webapp.controllers;

import com.getjavajob.dzavorin.common.model.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@RestController
public class XMLController {

    private static final Logger logger = LogManager.getLogger(XMLController.class);

    @GetMapping("/auth/downloadXML")
    public ResponseEntity<byte[]> downloadXML(@SessionAttribute("account") Account account) throws IOException {
        try {
            logger.debug(account.getId() + " is downloading xml representation");
            JAXBContext context = JAXBContext.newInstance(Account.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
            m.marshal(account, xmlStream);
            byte[] xml = xmlStream.toByteArray();
            HttpHeaders responseHeaders =  new HttpHeaders();
            responseHeaders.set("charset", "utf-8");
            responseHeaders.setContentType(new MediaType("application", "xml"));
            responseHeaders.set("Content-Disposition", "attachment; filename=account.xml");
            responseHeaders.setContentLength(xml.length);
            return new ResponseEntity<>(xml, responseHeaders, HttpStatus.OK);
        } catch (JAXBException e) {
            logger.error("failed to download to file");
        }
        return null;
    }

    @PostMapping("/auth/uploadXML")
    public ModelAndView uploadXML(@RequestParam("xmlFile") MultipartFile xmlFile) {
        Account accountfromXml = null;
        ModelAndView mav = new ModelAndView("editUser");
        try {
            JAXBContext context = JAXBContext.newInstance(Account.class);
            Unmarshaller un = context.createUnmarshaller();
            File file = new File(xmlFile.getOriginalFilename());
            xmlFile.transferTo(file);
            accountfromXml = (Account) un.unmarshal(file);
        } catch (JAXBException | IOException e) {
            logger.error("Xml upload failed with: " + e.toString());
        } catch (IllegalArgumentException e) {
            logger.error("not valid xsd schema");
        }
        if (accountfromXml == null) {
            return mav;
        }
        logger.debug("passing account: " + accountfromXml.toString());
        mav.addObject("accountfromXml", accountfromXml);
        return mav;
    }
}
