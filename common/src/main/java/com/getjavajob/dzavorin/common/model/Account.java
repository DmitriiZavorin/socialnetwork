package com.getjavajob.dzavorin.common.model;

import com.getjavajob.dzavorin.common.adapters.LocalDateAdapter;
import com.getjavajob.dzavorin.common.adapters.LocalDateAttributeConverter;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ACCOUNTS", uniqueConstraints = {@UniqueConstraint(columnNames = "EMAIL")})
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements BaseEntity {

    private static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACCOUNT_ID", nullable = false)
    @XmlAttribute
    private long id;

    @Column(name = "FIRST_NAME")
    @XmlElement
    private String firstName;

    @Column(name = "SECOND_NAME")
    @XmlElement
    private String secondName;

    @Column(name = "LAST_NAME")
    @XmlElement
    private String lastName;

    @Column(name = "BIRTH_DATE")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate dateOfBirth;

    @Column(name = "WORK_PHONE")
    @XmlElement
    private String workPhone;

    @Column(name = "HOME_ADDRESS")
    @XmlElement
    private String homeAddress;

    @Column(name = "WORK_ADDRESS")
    @XmlElement
    private String workAddress;

    @Column(name = "EMAIL", nullable = false)
    @XmlElement
    private String email;

    @Column(name = "ICQ")
    @XmlElement
    private String icq;

    @Column(name = "SKYPE")
    @XmlElement
    private String skype;

    @Column(name = "INFO")
    @XmlElement
    private String additionalInfo;

    @Column(name = "PASSWORD", nullable = false)
    @XmlTransient
    private String password;

    @Lob
    @Column(name = "PHOTO", length = 15000)
    @XmlTransient
    private byte[] photo;

    @Column(name = "ROLE", nullable = false)
    @Enumerated(EnumType.STRING)
    @XmlTransient
    private Role role;

    @Singular
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name="ACCOUNT_PHONES", joinColumns = @JoinColumn(name="ACCOUNT_ID"))
    @Column(name="PHONE")
    @XmlElementWrapper(name = "phones")
    @XmlElement(name = "phone")
    private List<String> phones = new ArrayList<>();

    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "ACCOUNT_FRIENDS",
            joinColumns = @JoinColumn(name="from_id"),
            inverseJoinColumns = @JoinColumn(name = "to_id"))
    private Set<Account> friends = new HashSet<>();


    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "ACCOUNT_FRIENDS",
            joinColumns = @JoinColumn(name="to_id"),
            inverseJoinColumns = @JoinColumn(name = "from_id"))
    private Set<Account> backFriends = new HashSet<>();

    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "FRIEND_REQUESTS",
            joinColumns = @JoinColumn(name="from_id"),
            inverseJoinColumns = @JoinColumn(name = "to_id"))
    private Set<Account> requests = new HashSet<>();

    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "FRIEND_REQUESTS",
            joinColumns = @JoinColumn(name="to_id"),
            inverseJoinColumns = @JoinColumn(name = "from_id"))
    private Set<Account> backRequests = new HashSet<>();

    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "MESSAGE_CONTACTS",
            joinColumns = @JoinColumn(name="from_id"),
            inverseJoinColumns = @JoinColumn(name = "to_id"))
    private Set<Account> contacts = new HashSet<>();

    @Singular
    @XmlTransient
    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "MESSAGE_CONTACTS",
            joinColumns = @JoinColumn(name="to_id"),
            inverseJoinColumns = @JoinColumn(name = "from_id"))
    private Set<Account> backContacts = new HashSet<>();

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Account account = (Account) o;

        return id == account.id;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + (workPhone != null ? workPhone.hashCode() : 0);
        result = 31 * result + (homeAddress != null ? homeAddress.hashCode() : 0);
        result = 31 * result + (workAddress != null ? workAddress.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (icq != null ? icq.hashCode() : 0);
        result = 31 * result + (skype != null ? skype.hashCode() : 0);
        result = 31 * result + (additionalInfo != null ? additionalInfo.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(photo);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (phones != null ? phones.hashCode() : 0);
        return result;
    }
}