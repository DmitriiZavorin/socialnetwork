package com.getjavajob.dzavorin.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MESSAGE")
public class Message implements BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MESSAGE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    private MessageType type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private Account sender;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RECIEVER_ID")
    private Account reciever;

    @Transient
    private String senderName;

    @Transient
    private Long senderId;

    @Transient
    private Long recieverId;

    @Column(name = "TEXT", nullable = false)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIME", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date time;

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", senderId='" + senderId + '\'' +
                ", recieverId='" + recieverId + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }
}