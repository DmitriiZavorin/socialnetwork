package com.getjavajob.dzavorin.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ACCOUNT_WALL_POSTS")
public class AccountWallPost implements BaseEntity {

    private static final long serialVersionUID = 2L;

    @Id
    @Column(name = "POST_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TEXT", nullable = false)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIME", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date time;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private Account author;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "OWNER_ID")
    private Account wallOwner;

    public Long getOwnerId() {
        return wallOwner.getId();
    }

}