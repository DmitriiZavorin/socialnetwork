package com.getjavajob.dzavorin.common.model;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class CurrentAccount extends User {

    private Account account;

    public CurrentAccount(Account account) {
        super(account.getEmail(), account.getPassword(), AuthorityUtils.createAuthorityList(account.getRole().toString()));
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public Long getId() {
        return account.getId();
    }

    public Role getRole() {
        return account.getRole();
    }

    @Override
    public String toString() {
        return "CurrentAccount{" +
                "account=" + account +
                '}';
    }
}
