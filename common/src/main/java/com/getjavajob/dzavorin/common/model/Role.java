package com.getjavajob.dzavorin.common.model;

public enum Role {

    USER, ADMIN
}
