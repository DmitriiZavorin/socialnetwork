package com.getjavajob.dzavorin.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WallPostDto implements BaseDto {

    private long id;
    //private long ownerId;
    private String time;
    private String text;
    private AccountDto author;
}
