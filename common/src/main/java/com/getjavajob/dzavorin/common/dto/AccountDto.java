package com.getjavajob.dzavorin.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountDto implements BaseDto {

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String photo;

}