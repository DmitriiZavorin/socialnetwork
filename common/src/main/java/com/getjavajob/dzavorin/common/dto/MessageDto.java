package com.getjavajob.dzavorin.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageDto implements BaseDto {

    private long senderId;
    private long recieverId;
    private String time;
    private String text;
}
