package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountDaoImpl extends JpaRepository<Account, Long> {

    Account getByEmail(String email);

    @Query("select ac from Account ac where lower(ac.firstName) " +
            "LIKE LOWER(CONCAT('%',:searchReq, '%')) OR lower(ac.lastName) LIKE LOWER(CONCAT('%',:searchReq, '%'))")
    Page<Account> getBySearchReq(@Param("searchReq") String searchReq, Pageable pageRequest);


    @Query("select ac from Account ac where lower(ac.firstName) " +
            "LIKE LOWER(CONCAT('%',:searchReq, '%')) OR lower(ac.lastName) LIKE LOWER(CONCAT('%',:searchReq, '%'))")
    List<Account> getBySearchReqAutocomplete(@Param("searchReq") String searchReq, Pageable pageable);

}