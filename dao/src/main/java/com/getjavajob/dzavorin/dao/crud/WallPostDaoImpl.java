package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.AccountWallPost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WallPostDaoImpl extends JpaRepository<AccountWallPost, Long> {

    List<AccountWallPost> findAllByWallOwnerOrderByIdDesc(Account wallOwner);

}