package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.*;

public interface MessageDaoImpl extends JpaRepository<Message, Long> {

    List<Message> getAllBySenderAndReciever(Account sender, Account reciever);

    default List<Message> getMessages(Account sender, Account reciever) {
        List<Message> mes = getAllBySenderAndReciever(sender, reciever);
        mes.addAll(getAllBySenderAndReciever(reciever, sender));
        mes.sort(Comparator.comparingLong(o -> o.getTime().getTime()));
        return mes;
    }
}