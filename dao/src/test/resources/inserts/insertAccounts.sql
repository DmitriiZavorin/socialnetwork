INSERT INTO ACCOUNTS (FIRST_NAME, SECOND_NAME, LAST_NAME, BIRTH_DATE,
                      WORK_PHONE, HOME_ADDRESS, WORK_ADDRESS, EMAIL, ICQ, SKYPE, INFO, PASSWORD, PHOTO)
VALUES ('Ivan', 'Ivanovich', 'Ivanov', '1956-02-23', '+79245678901', 'some work address ivan',
                'some home address ivan', 'ivan56@gmail.com', '1234567', 'ivan56', 'some info Ivan', 'qwerty', NULL),
  ('Dima', 'Dmitrievich', 'Dmitrov', '1978-04-11', '+79871234567', 'some work address dima',
           'some home address dima', 'dima78@mail.ru', '987654', 'dimaDima', 'some info Dima', 'asdf123', NULL),
  ('Petr', 'Petrovich', 'Petrov', '1992-08-30' , '+79564945725', 'some work address petr',
           'some home address petr', 'petrPetrov@yandex.ru', '34534562', 'petrovich92', 'some info Petr', '567', NULL),
  ('Masha', 'Alexandrovna', 'Alexandrova', '1986-03-21', '+79564945725', 'some work address masha',
            'some home address masha', 'mashaSA@ffff.ru', '3542356', 'masha312', 'some info Masha', '123', NULL),
  ('Dasha', 'Alexevna', 'Alexeeva', '1995-07-14', '+79563456436', 'some work address dasha',
            'some home address dasha', 'dasha95@ffff.ru', '3543453', 'dashka95', 'some info Dasha', '54321', NULL);