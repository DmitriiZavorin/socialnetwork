package com.getjavajob.dzavorin.dao;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import org.junit.*;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = JpaConfig.class)
@Sql(value = {"/schema.sql", "/data.sql"})
public class AccountDaoTest {

    @Autowired
    private AccountDaoImpl accountDao;

    private Account accountOne;
    private Account accountTwo;
    private Account accountThree;

    @Before
    public void create() {
        accountOne = Account.builder()
                .id(1)
                .firstName("Ivan")
                .email("ivan56@gmail.com")
                .password("qwerty")
                .build();
        accountOne.setPhones(asList("+71234567890"));
        accountTwo = Account.builder()
                .id(2)
                .firstName("Dima")
                .email("dima78@mail.ru")
                .password("asdf123")
                .build();
        accountTwo.setPhones(asList("+74951234567", "+789636375843"));
        accountThree = Account.builder()
                .id(3)
                .firstName("Petr")
                .email("petrPetrov@yandex.ru")
                .password("567")
                .build();
        accountThree.setPhones(asList("+9012345678"));
    }

    @Test
    public void getById_Exist() {
        Account account = accountDao.findOne(1L);
        assertEquals("Ivan", account.getFirstName());
    }

    @Test(expected = NullPointerException.class)
    public void getById_Fail() {
        Account account = accountDao.findOne(6L);
        assertEquals("Ivan", account.getFirstName());
    }

    @Test
    public void getAll_OK() {
        List<Account> accounts = accountDao.findAll();
        assertEquals(5, accounts.size());
    }

    @Test
    public void update_OK() {
        accountTwo.setFirstName("Ilya");
        assertEquals("Ilya", accountDao.save(accountTwo).getFirstName());
    }

    @Test
    public void searchByRequest_OK() {
        Page<Account> searched = accountDao.getBySearchReq("i", new PageRequest(0, 5));
        System.out.println(searched.getContent().get(0).getLastName());
        assertEquals(2, searched.getContent().size());
    }

    @Test
    public void searchByRequest_Fail() {
        Page<Account> searched = accountDao.getBySearchReq("a", new PageRequest(0, 10));
        assertNotEquals(3, searched.getContent().size());
    }

    @Test
    public void searchByRequestAutocomplete_OK() {
        List<Account> searched = accountDao.getBySearchReqAutocomplete("m", new PageRequest(0, 10));
        assertEquals(2, searched.size());
    }

    @Test
    public void searchByRequestAutocomplete_Fail() {
        List<Account> searched = accountDao.getBySearchReqAutocomplete("e", new PageRequest(0, 5));
        assertNotEquals(5, searched.size());
    }

    @Test
    public void getByEmail_OK() {
        assertEquals(accountTwo.getEmail(), accountDao.getByEmail("dima78@mail.ru").getEmail());
    }

    @Test
    public void getByEmail_Fail() {
        assertNotEquals(accountTwo.getEmail(), accountDao.getByEmail("ivan56@gmail.com").getEmail());
    }
}