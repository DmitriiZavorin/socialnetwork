### Project "SocialNetwork"  

**Functionality:**  
+ registration    
+ authentication  
+ ajax search with pagination and autocomplete  
+ display profile  
+ edit profile  
+ upload and download account xml representation  
+ ajax account wall with posts  
+ private messages and chat with WebSocket  

**Tools:**  
JDK 8, SpringBoot, Spring Data, SpringSecurity / Jackson, jQuery, Bootstrap, SockJS, JUnit, Mockito / Maven, Bitbucket, Tomcat, IntelliJIDEA.  

**Notes:**  
SQL ddl is located in `db/ddl.sql`

**Screenshots:**  
![Login page](screenshots/login.png)  
![Registration](screenshots/registration.png)  
![Home page](screenshots/home.png)  
![Friends](screenshots/friends.png) 
![Private messages](screenshots/messages.png) 
![Search form](screenshots/search.png)   
![Chat](screenshots/chat.png) 

_Project hosted at https://gjjsocial.herokuapp.com_  
_login: elon@gmail.com_  
_pass: 123_

__    
**Zavorin Dmitrii**      
Training getJavaJob     
http://www.getjavajob.com       