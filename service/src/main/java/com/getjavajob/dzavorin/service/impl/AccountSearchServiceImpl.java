package com.getjavajob.dzavorin.service.impl;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.dto.AccountDto;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.service.SearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;

import java.util.stream.Collectors;

@Service
public class AccountSearchServiceImpl implements SearchService<Account, AccountDto> {

    private static final Logger logger = LogManager.getLogger(AccountSearchServiceImpl.class);

    @Value("${search.paginationLimit}")
    private int paginationLimit;
    @Value("${search.autocompleteLimit}")
    private int autocompleteLimit;

    private AccountDaoImpl accountDao;

    @Autowired
    public AccountSearchServiceImpl(AccountDaoImpl accountDao) {
        this.accountDao = accountDao;
    }

    public List<AccountDto> getNamesBySearchReq(String searchReq) {
        return accountDao.getBySearchReqAutocomplete(searchReq, new PageRequest(0, autocompleteLimit))
                .stream().map(a -> AccountDto.builder()
                        .id(a.getId())
                        .firstName(a.getFirstName())
                        .lastName(a.getLastName())
                        .build())
                .collect(Collectors.toList());
    }

    public Page<Account> getBySearchReq(String searchReq, int page) {
        System.out.println("pagination limit:" + paginationLimit);
        return accountDao.getBySearchReq(searchReq, new PageRequest(page, paginationLimit));
    }

}