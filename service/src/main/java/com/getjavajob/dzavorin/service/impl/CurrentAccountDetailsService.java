package com.getjavajob.dzavorin.service.impl;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.CurrentAccount;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CurrentAccountDetailsService implements UserDetailsService {

    private static org.apache.logging.log4j.Logger logger = LogManager.getLogger(CurrentAccountDetailsService.class);
    private AccountServiceImpl accountServiceImpl;

    @Autowired
    public CurrentAccountDetailsService(AccountServiceImpl accountServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
    }

    @Override
    public CurrentAccount loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.warn("Authenticating user with email={}", email.replaceFirst("@.*", "@***"));
        Account account = accountServiceImpl.getUserByEmail(email);
        if (account == null) {
            throw new UsernameNotFoundException(String.format("User with email=%s was not found", email));
        }
        return new CurrentAccount(account);
    }

}
