package com.getjavajob.dzavorin.service.impl;

import com.getjavajob.dzavorin.common.dto.MessageDto;
import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.Message;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.MessageDaoImpl;
import com.getjavajob.dzavorin.service.MessageService;
import com.getjavajob.dzavorin.service.utils.MessageDtoMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountMessageServiceImpl implements MessageService<Account, MessageDto> {

    private static final Logger logger = LogManager.getLogger(AccountMessageServiceImpl.class);

    private AccountDaoImpl accountDao;
    private MessageDaoImpl messageDao;

    @Autowired
    public AccountMessageServiceImpl(AccountDaoImpl accountDao, MessageDaoImpl messageDao) {
        this.accountDao = accountDao;
        this.messageDao = messageDao;
    }

    public List<MessageDto> getMessages(Account sender, Account reciever) {
        List<Message> messages = messageDao.getMessages(sender, reciever);
        logger.error(messages.isEmpty());
        return MessageDtoMapper.mapList(messages);
    }

    public MessageDto saveMessage(Message message) {
        message = Message.builder()
                    .sender(accountDao.findOne(message.getSenderId()))
                    .reciever(accountDao.findOne(message.getRecieverId()))
                    .text(message.getText())
                    .build();
        return MessageDtoMapper.map(messageDao.save(message));
    }
}