package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dto.BaseDto;
import com.getjavajob.dzavorin.common.model.BaseEntity;

import java.util.List;

public interface WallPostService<W extends BaseEntity, V extends BaseDto> {

    List<V> getPosts(W w);

    V addPost(W w, long id, String text);

    void deletePost(long id);

}