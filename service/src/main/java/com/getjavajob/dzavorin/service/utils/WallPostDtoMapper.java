package com.getjavajob.dzavorin.service.utils;

import com.getjavajob.dzavorin.common.model.AccountWallPost;
import com.getjavajob.dzavorin.common.dto.WallPostDto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WallPostDtoMapper {

    public static List<WallPostDto> mapToList(List<AccountWallPost> wallPost) {
        return wallPost.stream()
                .map(w -> WallPostDto.builder()
                        .id(w.getId())
                        .author(AccountDtoMapper.map(w.getAuthor()))
                        .text(w.getText())
                        .time(w.getTime().toString())
                        .build())
                .collect(Collectors.toList());
    }

    public static WallPostDto map(AccountWallPost accountWallPost) {
        return WallPostDto.builder()
                .id(accountWallPost.getId())
                .author(AccountDtoMapper.map(accountWallPost.getAuthor()))
                .time(accountWallPost.getTime() == null ? new Date().toString() : accountWallPost.getTime().toString())
                .text(accountWallPost.getText())
                .build();
    }
}