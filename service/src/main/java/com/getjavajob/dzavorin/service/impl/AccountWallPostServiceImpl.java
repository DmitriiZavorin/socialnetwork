package com.getjavajob.dzavorin.service.impl;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.model.AccountWallPost;
import com.getjavajob.dzavorin.common.dto.WallPostDto;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.WallPostDaoImpl;
import com.getjavajob.dzavorin.service.WallPostService;
import com.getjavajob.dzavorin.service.utils.WallPostDtoMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountWallPostServiceImpl implements WallPostService<Account, WallPostDto> {

    private static final Logger logger = LogManager.getLogger(AccountWallPostServiceImpl.class);

    private WallPostDaoImpl wallPostDao;
    private AccountDaoImpl accountDao;

    @Autowired
    public AccountWallPostServiceImpl(WallPostDaoImpl wallPostDao, AccountDaoImpl accountDao) {
        this.wallPostDao = wallPostDao;
        this.accountDao = accountDao;
    }

    public AccountWallPost getWallPostById(long id) {
        return wallPostDao.findOne(id);
    }

    public List<WallPostDto> getPosts(Account account) {
        logger.debug("retrieving posts for account " + account.getId());
        return WallPostDtoMapper.mapToList(wallPostDao.findAllByWallOwnerOrderByIdDesc(account));
    }

    @Override
    public WallPostDto addPost(Account account, long ownerId, String text) {
        AccountWallPost wallPost = AccountWallPost.builder()
                .text(text)
                .wallOwner(accountDao.findOne(ownerId))
                .author(accountDao.findOne(account.getId()))
                .build();
        logger.debug("adding post for account " + account.getId() + ", ownerId " + wallPost.getOwnerId());
        return WallPostDtoMapper.map(wallPostDao.save(wallPost));
    }

    public void deletePost(long postId) {
        logger.debug("deleting post " + postId);
        wallPostDao.delete(postId);
    }

}