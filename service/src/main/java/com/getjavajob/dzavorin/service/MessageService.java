package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dto.BaseDto;
import com.getjavajob.dzavorin.common.model.BaseEntity;
import com.getjavajob.dzavorin.common.model.Message;

import java.util.List;

public interface MessageService<E extends BaseEntity, I extends BaseDto> {

    List<I> getMessages(E sender, E reciever);

    I saveMessage(Message message);
}