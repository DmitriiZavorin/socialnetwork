package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.model.Account;

public interface BaseEntityService<E> {

    E getById(long id);

    E create(E e);

    E update(E oldE, Account newE);
}