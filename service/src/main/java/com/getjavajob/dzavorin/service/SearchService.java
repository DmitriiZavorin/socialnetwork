package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dto.BaseDto;
import com.getjavajob.dzavorin.common.model.BaseEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SearchService<E extends BaseEntity, I extends BaseDto> {

    List<I> getNamesBySearchReq(String searchReq);

    Page<E> getBySearchReq(String searchReq, int page);
}