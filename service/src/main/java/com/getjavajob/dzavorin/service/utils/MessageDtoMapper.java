package com.getjavajob.dzavorin.service.utils;

import com.getjavajob.dzavorin.common.dto.MessageDto;
import com.getjavajob.dzavorin.common.model.Message;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MessageDtoMapper {

    public static List<MessageDto> mapList(List<Message> messages) {
        return messages.stream().map(MessageDtoMapper::map).collect(Collectors.toList());
    }

    public static MessageDto map(Message message) {
        return MessageDto.builder()
                .recieverId(message.getReciever().getId())
                .senderId(message.getSender().getId())
                .text(message.getText())
                .time(message.getTime() == null ? new Date().toString() : message.getTime().toString())
                .build();
    }
}