package com.getjavajob.dzavorin.service.utils;

import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.common.dto.AccountDto;

import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AccountDtoMapper {

    public static AccountDto map(Account a) {
        return AccountDto.builder()
                .id(a.getId())
                .firstName(a.getFirstName())
                .lastName(a.getLastName())
                .photo(Base64.getEncoder().encodeToString(a.getPhoto()))
                .email(a.getEmail())
                .build();
    }

    public static List<AccountDto> mapToList(List<Account> account) {
        return account.stream()
                .map(a -> AccountDto.builder()
                        .id(a.getId())
                        .firstName(a.getFirstName())
                        .lastName(a.getLastName())
                        .photo(Base64.getEncoder().encodeToString(a.getPhoto()))
                        .email(a.getEmail())
                        .build())
                .collect(Collectors.toList());
    }

    public static Set<AccountDto> mapToSet(Set<Account> account) {
        return account.stream()
                .map(a -> AccountDto.builder()
                        .id(a.getId())
                        .firstName(a.getFirstName())
                        .lastName(a.getLastName())
                        .photo(Base64.getEncoder().encodeToString(a.getPhoto()))
                        .email(a.getEmail())
                        .build())
                .collect(Collectors.toSet());
    }
}
