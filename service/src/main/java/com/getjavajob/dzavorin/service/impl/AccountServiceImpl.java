package com.getjavajob.dzavorin.service.impl;

import com.getjavajob.dzavorin.common.dto.AccountDto;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.common.model.Account;
import com.getjavajob.dzavorin.service.BaseEntityService;
import com.getjavajob.dzavorin.service.utils.AccountDtoMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.getjavajob.dzavorin.service.utils.AccountDtoMapper.mapToSet;

@Service
public class AccountServiceImpl implements BaseEntityService<Account> {

    private static final Logger logger = LogManager.getLogger(AccountServiceImpl.class);
    private static final int MAX_FRIENDS = 1000;

    private AccountDaoImpl accountDao;

    @Autowired
    public AccountServiceImpl(AccountDaoImpl accountDao) {
        this.accountDao = accountDao;
    }

    public Account create(Account account) {
        return accountDao.save(account);
    }

    public Account getById(long id) {
        return accountDao.findOne(id);
    }

    public Account update(Account oldAccount, Account newAccount) {
        newAccount.setId(oldAccount.getId());
        if (newAccount.getPhoto() == null || newAccount.getPhoto().length == 0) {
            logger.debug("no photo on account edit");
            newAccount.setPhoto(oldAccount.getPhoto());
        }
        newAccount.setPassword(BCrypt.hashpw(newAccount.getPassword(), BCrypt.gensalt(12)));
        return accountDao.save(newAccount);
    }

    public Account addFriend(Account from, Account to) {
        if (from.getFriends().size() < MAX_FRIENDS && to.getFriends().size() < MAX_FRIENDS) {
            from.getRequests().remove(to);
            from.getBackRequests().remove(to);
            from.getFriends().add(to);
            return accountDao.save(from);
        }
        return null;
    }

    public Account addRequest(Account from, Account to) {
        from.getRequests().add(to);
        return accountDao.save(from);
    }

    public Account deleteRequest(Account from, Account to) {
        from.getRequests().remove(to);
        from.getBackRequests().remove(to);
        return accountDao.save(from);
    }

    public Account deleteFriend(Account from, Account to) {
        from.getFriends().remove(to);
        from.getBackFriends().remove(to);
        return accountDao.save(from);
    }

    public Set<AccountDto> getFriends(Account account) {
        Set<Account> friends = account.getFriends();
        friends.addAll(account.getBackFriends());
        return mapToSet(friends);
    }

    public Set<AccountDto> getFriendsRequests(Account account) {
        return mapToSet(account.getBackRequests());
    }

    public Set<AccountDto> getOwnRequests(Account account) {
        return mapToSet(account.getRequests());
    }

    public Account getUserByEmail(String email) {
        return accountDao.getByEmail(email);
    }

    public Set<AccountDto> getContacts(Account account) {
        Set<AccountDto> contacts = AccountDtoMapper.mapToSet(account.getContacts());
        Set<AccountDto> backContacts = AccountDtoMapper.mapToSet(account.getBackContacts());
        contacts.addAll(backContacts);
        return contacts;
    }

    public AccountDto addContact(Long userId, Account account) {
        Account user = accountDao.findOne(userId);
        Account acc = accountDao.findOne(account.getId());
        acc.getContacts().add(user);
        accountDao.save(acc);
        accountDao.save(user);
        return AccountDtoMapper.map(user);
    }
}